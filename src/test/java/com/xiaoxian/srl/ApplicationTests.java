package com.xiaoxian.srl;

import com.xiaoxian.srl.service.IUserService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class ApplicationTests {

    @Autowired
    IUserService userService;

    @Test
    void list() {
        System.out.println(userService.list());
    }
}
