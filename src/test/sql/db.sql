/*
    Schema：test
    Info：测试数据库
    Date：2022/09/07 13:49:38
*/
-- 1、创建数据库
CREATE
DATABASE `test` CHARACTER SET 'utf8' COLLATE 'utf8_general_ci';

-- 2、创建表
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`
(
    `id`   bigint(11) NOT NULL AUTO_INCREMENT,
    `name` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- 3、插入数据
INSERT INTO `user`
VALUES (1, '赵');
INSERT INTO `user`
VALUES (2, '钱');
INSERT INTO `user`
VALUES (3, '孙');
INSERT INTO `user`
VALUES (4, '李');