package com.xiaoxian.srl.aop;

import com.xiaoxian.srl.annotation.RequestLimit;
import com.xiaoxian.srl.exception.RequestLimitException;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.concurrent.TimeUnit;

/*
 * @author      :xiaoixan
 * @date        :2022/07/08 21:42
 * @class       :RequestLimitAspect
 * @description :统计规定时间段内接口访问次数限制
 * @version     :1.0
 **/
@Slf4j
@Aspect
@Component
public class RequestLimitAspect {

    @Autowired
    private RedisTemplate redisTemplate;

    @Before("within(@org.springframework.web.bind.annotation.RestController *) && @annotation(requestLimit)")
    public void requestLimit(final JoinPoint joinPoint, RequestLimit requestLimit) throws RequestLimitException {
        ServletRequestAttributes requestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = requestAttributes.getRequest();
        // 127.0.0.1
        String ip = request.getRemoteAddr();
        // /user/list
        String uri = request.getRequestURI().toString();
        // http://127.0.0.1:8888/user/list
        String url = request.getRequestURL().toString();
        String key = "request-limit-" + url;
        long count = redisTemplate.opsForValue().increment(key, 1);
        if (count == 1) {
            redisTemplate.expire(key, requestLimit.timeout(), TimeUnit.MILLISECONDS);
        }
        if (count > requestLimit.maxCount()) {
            String error = "HTTP请求【" + url + "】超过了限定的访问次数【" + requestLimit.maxCount() + "】";
            log.error(error);
            throw new RequestLimitException(error);
        }
    }
}
