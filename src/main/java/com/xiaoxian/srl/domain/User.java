package com.xiaoxian.srl.domain;

import lombok.Data;

/*
 * @author      :xiaoixan
 * @date        :2022/07/08 21:30
 * @class       :User
 * @description :用户实体类
 * @version     :1.0
 **/
@Data
public class User {
    Long id;
    String name;
}
