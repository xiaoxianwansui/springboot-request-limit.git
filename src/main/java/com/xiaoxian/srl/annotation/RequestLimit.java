package com.xiaoxian.srl.annotation;

import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;

import java.lang.annotation.*;


/*
 * @author      :xiaoixan
 * @date        :2022/07/08 21:42
 * @class       :RequestLimit
 * @description :统计规定时间段内接口访问次数限制
 * @version     :1.0
 **/
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@Documented
@Order(Ordered.HIGHEST_PRECEDENCE)
public @interface RequestLimit {

    /**
     * @param :
     * @return :int
     * @author :xiaoixan
     * @date :2022/07/10 10:57
     * @description :访问最大次数
     **/
    public int maxCount() default Integer.MAX_VALUE;

    /**
     * @param :
     * @return :long
     * @author :xiaoixan
     * @date :2022/07/10 10:57
     * @description :访问时间段（默认1分钟）
     **/
    public long timeout() default 60 * 1000;
}
