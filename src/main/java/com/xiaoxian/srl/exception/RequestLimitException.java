package com.xiaoxian.srl.exception;

/*
 * @author      :xiaoixan
 * @date        :2022/07/10 12:34
 * @class       :RequestLimitException
 * @description :TODO
 * @version     :1.0
 **/
public class RequestLimitException extends Exception {
    public RequestLimitException() {
        super("HTTP请求超过了限定的访问次数");
    }

    public RequestLimitException(String message) {
        super(message);
    }
}
