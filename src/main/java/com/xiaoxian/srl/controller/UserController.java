package com.xiaoxian.srl.controller;

import com.xiaoxian.srl.annotation.RequestLimit;
import com.xiaoxian.srl.domain.User;
import com.xiaoxian.srl.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/*
 * @author      :xiaoixan
 * @date        :2022/07/08 21:42
 * @class       :UserController
 * @description :用户管理
 * @version     :1.0
 **/
@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    IUserService userService;

    /**
     * @param :
     * @return :java.util.List<com.xiaoxian.sdd.domain.User>
     * @author :xiaoixan
     * @date :2022/07/09 12:44
     * @description :获取用户列表
     **/
    @RequestLimit(maxCount = 2)
    @GetMapping("/list")
    public List<User> list() {
        return userService.list();
    }
}
