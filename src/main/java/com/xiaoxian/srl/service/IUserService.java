package com.xiaoxian.srl.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xiaoxian.srl.domain.User;

import java.util.List;

/*
 * @author      :xiaoixan
 * @date        :2022/07/08 21:30
 * @class       :IUserService
 * @description :用户服务类
 * @version     :1.0
 **/
public interface IUserService extends IService<User> {
    List<User> list();
}
