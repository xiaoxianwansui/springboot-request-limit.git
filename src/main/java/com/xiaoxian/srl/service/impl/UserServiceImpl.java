package com.xiaoxian.srl.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xiaoxian.srl.domain.User;
import com.xiaoxian.srl.mapper.UserMapper;
import com.xiaoxian.srl.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/*
 * @author      :xiaoixan
 * @date        :2022/07/08 21:41
 * @class       :UserServiceImpl
 * @description :用户服务实现类
 * @version     :1.0
 **/
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements IUserService {
    @Autowired
    private UserMapper userMapper;

    @Override
    public List<User> list() {
        return userMapper.selectList(new QueryWrapper<>());
    }
}
