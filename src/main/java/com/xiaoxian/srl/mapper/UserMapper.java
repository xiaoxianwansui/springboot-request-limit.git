package com.xiaoxian.srl.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiaoxian.srl.domain.User;

/*
 * @author      :xiaoixan
 * @date        :2022/07/08 21:30
 * @class       :UserMapper
 * @description :用户映射类
 * @version     :1.0
 **/
public interface UserMapper extends BaseMapper<User> {

}